/*
Initialisiere eine Map “student”, die unter dem Key “name” den Namen des Studenten, 
unter dem Key “age” das Alter des Studenten und unter dem Key “grades”
eine Liste der Noten des Studenten enthält.

- Gebe die einzelnen Key-Value-Einträge in der Konsole aus (Optional: Anhand einer Schleife)
- Füge zur Liste “grades” eine neue Note hinzu und gebe die Liste erneut in der Konsole aus
- Gerne auch GStrings verwenden
*/

//TODO