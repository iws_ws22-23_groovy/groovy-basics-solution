/*
Definiert ist eine Map mit den Namen "person" und mit den Keys "name", "alter" und "stadt".

- Verwenden Sie den Elvis-Operator, um einen Standardwert für das Alter festzulegen,
    für den Fall,dass es in der Map mit null initialisiert wird. Der Standardwert sollte 30 sein.
- Verwende den Operator Spread, um die Werte einer zweiten Map namens "address" in die Map 
    "person" einzufügen. Die Map "address" sollte die Keys "straße" und "hausnummer" enthalten.
- Gebe die resultierende Map in der Konsole aus.
*/

def person = [name: 'Hans-Joachim', alter: 43, stadt: 'Mannheim']
//TODO