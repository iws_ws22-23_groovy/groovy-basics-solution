/*
Löse die folgende Aufgabe ohne if-Abfragen: 
- Die Methode getStudentenName(String matrNr) soll den Namen eines Studenten 
    anhand der Übergabe seiner Matrikelnummer zurückgeben können. 
- Die eingetragenen Studenten sind in der Map studenten in der Methode vorzufinden. 
- Falls eine ungültige oder eine nicht zugeteilte Matrikelnummer übergeben wird, 
    soll folgendes zurückgegeben werden: “Student mit der Matrikelnummer hierEingabe existiert nicht.” 
- Im besten Fall ist die Methode zwei bis drei Zeilen lang.

Tipp: Nutze dabei die für Maps in Groovy bestehende withDefault(Closure closure) Methode.
- withDefault: https://docs.groovy-lang.org/latest/html/groovy-jdk/java/util/Map.html#withDefault(boolean,%20boolean,%20groovy.lang.Closure)
*/

def getStudentenName(String matrNr){
    //TODO
    def m = [1823475: "Hans", 3003701: "Max"]
}