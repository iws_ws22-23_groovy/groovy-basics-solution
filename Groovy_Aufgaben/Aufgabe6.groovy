/*
Schreibe eine Methode, die einen Termin zum Terminblock zweier Person hinzufügt,
falls diese keinen bereits bestehenden Termin zu dieser Zeit besitzen. 
Einfachshalber betrachten wir hierbei im Stundentakt einen Arbeitstag von 08:00 Uhr bis 17:00 Uhr.
Die Methode bekommt drei Parameter übergeben: Die Termine der Personen jeweils als 
2-dimensionales Array und der hinzuzufügende Termin als eine Liste bestehend aus 
einem Start- und Endzeitpunkt. Kann der Termin bei beiden Personen eingetragen werden,
soll dieser zur Liste beider Personen hinzugefügt und in der Konsole "Termin wurde hinzugefügt."
ausgegeben werden. Andernfalls soll in der Konsole 
"Termin konnte nicht hinzugefügt, da Person1 (optional: und Person2) keine Zeit haben."
ausgegeben werden.
*/

def fuegeTerminHinzu(person1Termine, person2Termine, termin) {
  def startTime = termin[0]
  def endTime = termin[1]
  //TODO
}

person1Termine = [[8,10],[14,17]]
person2Termine = [[8,10],[15,17]]
neuerTermin = [13,15]

fuegeTerminHinzu(person1Termine, person2Termine, neuerTermin)