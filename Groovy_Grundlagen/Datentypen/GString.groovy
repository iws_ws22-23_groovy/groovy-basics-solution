//GString
def topic = "Groovy"

//Variablen aufrufen
println "I am learning $topic"

//Objekte einbinden
println "It is ${new Date()}, and I am learning $topic"

//Operationen
println "The result of 1 + 1 is ${1 + 1}"

//Multiline GString
def message = """
Hello, This module includes the Topic $topic.
The workshop costs \$30.
"""