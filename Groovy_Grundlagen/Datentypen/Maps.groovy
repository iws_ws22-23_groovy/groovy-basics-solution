//Maps
def notenMap = [PR1: 2.3, PR2: 1.0]
println notenMap.getClass()

//Zugriff
println "Your grade for PR1 is ${notenMap['PR1']}"

//Schreiben und entfernen
notenMap['PR2'] = 2.7
notenMap['PR3'] = "1.0"
notenMap.remove('PR3')

//Abfrage
if 'PR2' in notenMap {
    println "Key 'PR2' existiert in der Map"
}

//Schleifen
notenMap.each { key, value -> 
    println "$key: $value"
}