//Ranges
def zahlenfolge = 1..10
def zahlenfolgeBis9 = 1..<10
def zahlenfolgeNegativ = 5..-10
def alphabetGroß = "A".."Z"
def alphabetKlein = "a".."z"
def alphabetRueckwaerts = "z".."a"

//Schleifen mit Ranges
zahlenfolgeBis9.each{ println it }
for(i in 1..4) print i

//Substring mit Ranges
def text = 'Groovy Grails DSL'
println text[0..5]
println text[0..5, 13..-1]

//Switch Case mit Ranges
def ageOld = 23
switch(ageOld) {
    case 0..12: println "Child" break
    case 13..18: println "Teenager" break
    case 19..25: println "Young Adult" break
    default: println "Adult"
}