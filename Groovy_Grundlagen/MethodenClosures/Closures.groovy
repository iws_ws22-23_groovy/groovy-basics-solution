//Closure in Variable speichern
def getFactorial = {num -> (num <=1 ? 1 : num * call(num-1))}
println getFactorial(4)

//Zugriff auf Variablen und Parameter
def greeting = "Hallo"
def sagHallo = {name -> (println "$greeting $name")}
sagHallo("Max")

//it-Parameter
def grades = ["PR1": 1.0, "PR2": 2.7]
grades.each {println "$it.key : $it.value"}

//Filtern durch Closure
def moduleList = ["Groovy", "Grails", "DSL"]
def groovyModule = moduleList.find { it == "Groovy"}
println groovyModule

//it überschreiben
def randNumList = [1, 2, 3, 4, 5, 6]
def numMatches = randNumList.findAll {item -> item > 4}
println numMatches

//Weitere Closures
println "Gibt es mind. eine Zahl > 5 : " + randNumList.any {item -> item > 5}
println "Sind alle Zahlen > 5 : " + randNumList.every {item -> item > 5}
println "Verdoppel : " + randNumList.collect {item -> item * 2}

//Closure als Parameter in einer Methode
static listEdit(list, clo){
    return list.findAll(clo)
}
def getEven = {num -> return(num % 2 == 0)}
def evenNums = listEdit(randNumList, getEven)
println "Evens: " + evenNums
