//Dynamische Parameter
def fügeZusammen(a, b) {
    a + b
}
println fügeZusammen(1, 2) 
println fügeZusammen("a", "b") 

//Default Parameter
def posEintrag(modulName, note='5.0') {
    "$modulName: $note!"
}
println posEintrag('PR2')
println posEintrag('PR2', 1.0)

//Dynamische Listen als Parameter
def rechneDurchschnittAus(... noten){
    noten.each{
        println it
    }
}
rechneDurchschnittAus(*[3,[4,4],"Hallo",1.7])