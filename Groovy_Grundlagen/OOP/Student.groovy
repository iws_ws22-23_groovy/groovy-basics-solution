class Student {
    //Field
    String name
    //Property
    private int matrikelNummer

    //Konstruktor
    Student(name,matrikelNummer) {
        this.name = name
        this.matrikelNummer = matrikelNummer
    }

    //Objektmethode
    def setMatrikelNummer(newMat){
        this.matrikelNummer = newMat
        println "Neue Matrikelnummer: $newMat"
    }
}