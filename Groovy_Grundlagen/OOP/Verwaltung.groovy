def student = new Student("Tim Schusterstudent", 332741)

//Generierte Setter-Methode
student.setName("Tim Musterstudent")

//Aufruf generierter Getter-Methoden
println "Name: ${student.getName()}"
println "Age: ${student.matrikelNummer}"

//Setter-Methode definiert in der Klasse Student wird aufgerufen
student.matrikelNummer = 432323

//Hinzufügen einer neuen Methode zur Laufzeit
student.metaClass.giveAllInfo = { -> println "Student: $delegate.name, $delegate.matrikelNummer" } 
student.giveAllInfo()