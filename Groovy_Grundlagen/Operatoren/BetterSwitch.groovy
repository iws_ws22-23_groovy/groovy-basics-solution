def x = 7
switch( x ) {
    case "Groovy":
        println "x is Groovy!"
        break
    case [5, 55, 555]:
        println "x is 5 or 55 or 555"
        break
    case 1..10:
        println "x is an Number between 1 and 10"
        break
    case Integer:
        println "Its an Integer"
        break
    case String:
        println "Its a String"
        break
    default:
        println "Something else"
}