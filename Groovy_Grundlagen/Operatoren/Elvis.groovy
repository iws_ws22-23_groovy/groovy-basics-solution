def name = "Groovy 101"
def numOfParticipants = null

//Elvis Operator
def displayNumOfParticipants = numOfParticipants ?: "Nicht definiert"

println("Module: ${name}, Number of participants ${displayNumOfParticipants}")

def modules = null
def defaultModule = ["Groovy 101", "Grails Advances"]

//Elvis Operator
def moduleList = modules ?: defaultModule

println moduleList