def prof1 = new Professor("Max Mustermann", [ComputerScience: ["PR1", "PR2"]])
def prof2 = new Professor("Mareike Musterfrau", null)

//Safe Navigation Operator
println prof1.facultyModules?.ComputerScience
println prof2.facultyModules?.ComputerScience