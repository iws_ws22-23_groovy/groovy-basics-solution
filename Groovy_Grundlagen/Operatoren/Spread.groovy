def modules = ["groovy","grails","dsl"]

//Objekte aus der Liste füllen die Parameterstellen
static displayThreeModules(String topic1, String topic2, String topic3){
    println "1st Module: $topic1"
    println "2nd Module: $topic2"
    println "3rd Module: $topic3"
}

//Listeinträge als Parameter mitgeben 
displayThreeModules(*modules)

def newModules = ["grails advanced", *modules]

//Objektmethode aufrufen
newModules = newModules*.toUpperCase();

static printModules(String... modules){
    println modules
}

printModules(*newModules)