//for-Schleife mit Ranges
for (i in 1..10) {
    println i
}

println "_____________________________"

//times{}-Closure
10.times {
    println "Groovy rocks"
}

println "_____________________________"

//each{}-Closure
["Groovy", "Grails", "DSL"].each {
    println it
}

println "_____________________________"

//eachWithIndex{}-Closure
["Groovy", "Grails", "DSL"].eachWithIndex { item, index ->
    println "$index : $item"
}