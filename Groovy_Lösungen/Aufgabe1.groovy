/*
Lass mithilfe von GStrings in der Konsole folgenden Text ausgeben:
[JetzigesDatumUndUhrzeit]: [name] nimmt an [moduleAnzahl] teil.
- [JetzigesDatumUndUhrzeit]: new Date() 
- [moduleAnzahl]: Erstelle eine Liste mit beliebig vielen Modulnamen als Einträge 
    und gebe für [moduleAnzahl] die Anzahl der Einträge der Liste aus
*/

def module = ["Groovy", "Grails", "DSL"]
def name = "Max"

println "${new Date()}: $name nimmt an ${module.size()} teil."