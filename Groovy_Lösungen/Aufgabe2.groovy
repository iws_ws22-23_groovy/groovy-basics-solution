/*
Schreibe ein Skript, das zu einem gegebenen Teilnehmernamen 
eine von drei Gruppennummern in der Konsole ausgibt.

- Alle Namen anfangend mit dem Buchstaben A bis G bekommen die Gruppennummer 1 zugewiesen.
- Namen mit dem Anfangsbuchstaben H bis P bekommen die Gruppennummer 2 
    und die restlichen Buchstaben die Gruppennummer 3 zugeteilt.
- Nutze dabei Ranges und ein Switch Case.
*/

def teilnehmername = "Hans-Joachim"
switch(teilnehmername[0]) {
    case "A".."G": 
        println 1
        break
    case "H".."P": 
        println 2
        break
    default: 
        println 3
    break
}