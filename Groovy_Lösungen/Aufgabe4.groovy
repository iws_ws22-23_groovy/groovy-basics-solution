/*
Definiert ist eine Map mit den Namen "person" und mit den Keys "name", "alter" und "stadt".

- Initialisiere eine zweite Map "adresse" mit den Keys "straße", "hausnummer".
-Verwenden Sie den Elvis-Operator, um einen Standardwert für das Alter festzulegen,
    für den Fall, dass es in der Map mit null initialisiert wird. Der Standardwert sollte 30 sein.
- Verwende den Operator Spread, um die Werte der zweiten Map "adresse" in die Map 
    "person" einzufügen.
- Gebe die resultierende Map in der Konsole aus.
*/

def person = [name: 'Hans-Joachim', alter: 43, stadt: 'Mannheim']
def adresse = [straße: 'Luisenalee', hausnummer: 42]
person.alter = person.alter ?: 30
person.putAll(*adresse)
println person