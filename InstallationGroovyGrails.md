﻿# Anleitung: Groovy & Grails Installation

Im Folgenden sind die Installationsschritte für Groovy und Grails aufgelistet. Beides wird für die Teilnahme am Groovy-Workshop vorausgesetzt.

# GNU/Linux & MacOS

In Unix-Systemen lassen sich Groovy & Grails in wenigen Schritten über den Package-Manager **SDKMAN!** einrichten. Hierfür muss dieser vorerst installiert werden:

Öffne ein neues Terminal und gebe ein:

    $ curl -s "https://get.sdkman.io" | bash

Folge den Anweisungen auf dem Bildschirm, um die Installation abzuschließen. Gebe dann folgenden Befehl im Terminal ein:

    $ source "$HOME/.sdkman/bin/sdkman-init.sh"

Führe abschließend den folgenden Befehl aus, um sicherzustellen, dass die Installation erfolgreich war:

    $ sdk version

Wenn alles gut gegangen ist, sollte die Version etwas so angezeigt werden:

      sdkman 5.16.0

Mit folgenden zwei Befehlen lässt sich Groovy und Grails installieren:

**Groovy:**

    $ sdk install groovy

Weitere Informationen: https://groovy.apache.org/download.html

**Grails:**

    $ sdk install grails

Weitere Informationen: https://grails.org/download.html

# Windows

Für Windows-Systeme müssen jeweils für Groovy und Grails die benötigten Binary-Dateien zur Installation heruntergeladen werden. Die Installation erfolgt durch das Eintragen der Pfade der entpackten Binary-Dateien in den Windows-Systemvariablen.

**Groovy**
Lade die letzte Version von Groovy als Binary herunter, unter folgendem Link: https://groovy.apache.org/download.html

Entpacke die heruntergeladene zip-Datei (am besten unter `C:\Program Files`)

**Grails**
Lade die letzte Version von Grails als Binary herunter, unter folgendem Link: [https://grails.org/download.html](https://grails.org/download.html)

Entpacke die heruntergeladene zip-Datei (am besten unter `C\:Program Files`)

**Path-Variablen hinzufügen**
Gebe die Pfade der `\bin`-Ordner der jeweils entpackten Groovy- und Grails-Ordner in 
1.  ![tut1.png](https://gitlab.com/iws_ws22-23_groovy/groovy_exercise_1_basics/-/raw/main/assets/tut1.png)
2. ![tut2.png](https://gitlab.com/iws_ws22-23_groovy/groovy_exercise_1_basics/-/raw/main/assets/tut2.png)
3. ![tut3.png](https://gitlab.com/iws_ws22-23_groovy/groovy_exercise_1_basics/-/raw/main/assets/tut3.png)
4. ![tut4.png](https://gitlab.com/iws_ws22-23_groovy/groovy_exercise_1_basics/-/raw/main/assets/tut4.png)
5. Ggf. muss der Rechner neugestartet werden







