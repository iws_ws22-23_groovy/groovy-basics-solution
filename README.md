# Die Programmiersprache Groovy

Die auf der Java Virtual Machine laufende Programmiersprache Groovy stellt eine verfeinerte Version der Java-Syntax dar und erweitert diese um viele weitere Funktionen, wie zum Beispiel Ranges und Closures. Mehr dazu findet man in der offiziellen Groovy-Dokumentation: https://groovy-lang.org/documentation.html 

Lösen Sie die Aufgaben, so "groovianisch" wie möglich!

## Aufgabe 1
Lass mithilfe von GStrings in der Konsole folgenden Text ausgeben: [JetzigesDatumUndUhrzeit]: [name] nimmt an [moduleAnzahl] teil.
- [JetzigesDatumUndUhrzeit]: new Date() 
- [moduleAnzahl]: Erstelle eine Liste mit beliebig vielen Modulnamen als Einträge und gebe für [moduleAnzahl] die Anzahl der Einträge der Liste aus

## Aufgabe 2
Schreibe ein Skript, das zu einem gegebenen Teilnehmernamen eine von drei Gruppennummern in der Konsole ausgibt.

- Alle Namen anfangend mit dem Buchstaben A bis G bekommen die Gruppennummer 1 zugewiesen.
- Namen mit dem Anfangsbuchstaben H bis P bekommen die Gruppennummer 2 und die restlichen Buchstaben die Gruppennummer 3 zugeteilt.
- Nutze dabei Ranges und ein Switch Case.

## Aufgabe 3
Initialisiere eine Map “student”, die unter dem Key “name” den Namen des Studenten, unter dem Key “age” das Alter des Studenten und unter dem Key “grades” eine Liste der Noten des Studenten enthält.

- Gebe die einzelnen Key-Value-Einträge in der Konsole aus (Optional: Anhand einer Schleife)
- Füge zur Liste “grades” eine neue Note hinzu und gebe die Liste erneut in der Konsole aus
- Gerne auch GStrings verwenden

## Aufgabe 4
Definiert ist eine Map mit den Namen "person" und mit den Keys "name", "alter" und "stadt".

- Verwenden Sie den Elvis-Operator, um einen Standardwert für das Alter festzulegen, für den Fall,dass es in der Karte mit null initialisiert wird. Der Standardwert sollte 30 sein.
- Verwende den Operator Spread, um die Werte einer zweiten Map namens "address" in die Map "person" einzufügen. Die Karte "address" sollte die Keys "straße" und "hausnummer" enthalten.
- Gebe die resultierende Map in der Konsole aus.

## Aufgabe 5
Löse die folgende Aufgabe ohne if-Abfragen: 
- Die Methode getStudentenName(String matrNr) soll den Namen eines Studenten anhand der Übergabe seiner Matrikelnummer zurückgeben können. 
- Die eingetragenen Studenten sind in der Map studenten in der Methode vorzufinden. 
- Falls eine ungültige oder eine nicht zugeteilte Matrikelnummer übergeben wird, soll folgendes zurückgegeben werden: “Student mit der Matrikelnummer hierEingabe existiert nicht.” 
- Im besten Fall ist die Methode zwei bis drei Zeilen lang.

Tipp: Nutze dabei die für Maps in Groovy bestehende withDefault(Closure closure) Methode.
- withDefault: https://docs.groovy-lang.org/latest/html/groovy-jdk/java/util/Map.html#withDefault(boolean,%20boolean,%20groovy.lang.Closure)

## Aufgabe 6 (Zusatz)
Schreibe eine Methode, die einen Termin zum Terminblock zweier Person hinzufügt, falls diese keinen bereits bestehenden Termin zu dieser Zeit besitzen. Einfachshalber betrachten wir hierbei im Stundentakt einen Arbeitstag von 08:00 Uhr bis 17:00 Uhr. Die Methode bekommt drei Parameter übergeben: Die Termine der Personen jeweils als 2-dimensionales Array und der hinzuzufügende Termin als eine Liste bestehend aus einem Start- und Endzeitpunkt. Kann der Termin bei beiden Personen eingetragen werden, soll dieser zur Liste beider Personen hinzugefügt und in der Konsole "Termin wurde hinzugefügt." ausgegeben werden. Andernfalls soll in der Konsole "Termin konnte nicht hinzugefügt, da Person1 (optional: und Person2) keine Zeit haben."
ausgegeben werden.

Tipp: Nutze dabei die every(Closure closure)-Methode, um mit den boolschen Ausdruck [it[1] <= startTime || it[0] >= endTime] zu schauen, ob der neue Termin in einem bestehendem Termin einer Person liegt.
